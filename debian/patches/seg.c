Description: add missing include file
From: Laszlo Kajan <lkajan@rostlab.org>
Forwarded: http://lists.alioth.debian.org/pipermail/debian-med-packaging/2012-July/016275.html

Index: seg-1994101801/seg.c
===================================================================
--- seg-1994101801.orig/seg.c	2012-07-04 18:15:49.000000000 +0000
+++ seg-1994101801/seg.c	2012-07-04 18:47:15.222835801 +0000
@@ -6,6 +6,7 @@
 
 /*--------------------------------------------------------------(includes)---*/
 
+#include <string.h>
 #include "genwin.h"
 #include "lnfac.h"
 
