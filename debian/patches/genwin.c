Description: add missing include file and fix type casts
From: Laszlo Kajan <lkajan@rostlab.org>
Forwarded: http://lists.alioth.debian.org/pipermail/debian-med-packaging/2012-July/016275.html

Index: seg-1994101801/genwin.c
===================================================================
--- seg-1994101801.orig/genwin.c	2012-07-04 18:15:40.000000000 +0000
+++ seg-1994101801/genwin.c	2012-07-04 18:48:41.414821989 +0000
@@ -5,6 +5,7 @@
 
 /*--------------------------------------------------------------(includes)---*/
 
+#include <string.h>
 #include "genwin.h"
 
 /*---------------------------------------------------------------(defines)---*/
@@ -53,7 +54,7 @@
 
 #define TESTMAX 1000
 void *tmalloc();
-int record_ptrs[TESTMAX] = {0,0,0,0};
+void *record_ptrs[TESTMAX] = {NULL,NULL,NULL,NULL};
 int rptr = 0;
 
 /*------------------------------------------------------------(genwininit)---*/
@@ -850,7 +851,7 @@
       exit(2);
      }
 
-   record_ptrs[rptr] = (int) ptr;
+   record_ptrs[rptr] = ptr;
    rptr++;
 
    return(ptr);
@@ -865,9 +866,9 @@
 
    for (i=0; i<rptr; i++)
      {
-      if (record_ptrs[i]==(int)ptr)
+      if (record_ptrs[i]==ptr)
         {
-         record_ptrs[i] = 0;
+         record_ptrs[i] = NULL;
          break;
         }
       }
